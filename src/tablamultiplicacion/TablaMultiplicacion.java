/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablamultiplicacion;

import java.util.Scanner;

/**
 *
 * @author ize
 */
public class TablaMultiplicacion {

    public static void main(String[] args) {
        //Creacion del objeto scanner
        Scanner entradaDato = new Scanner(System.in);
        
        //creacion de dos varibles
        int numero;
        int i,j;
        
        //Entrada de datos
        System.out.println("Indica la tabla de multiplicar a realizar: ");
        //Asignacion del objeto a la variable
        numero = entradaDato.nextInt();
        
        //For para ciclar cada iteracion de las tablas
        for(i = 1; i <= numero; i++){
            
            //Ciclo for para la multiplicacion de variables
            for(j = 1; j <= 10; j++){
                
                //Imprimir mensaje
                System.out.println(i +" x " + j + " = " + i * j);
            }
            //Salto de linea para cada itareacion de cada tabla
            System.out.println("");
        }
    }
    
}
